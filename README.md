# **Accept The Rules**

[//]: # (icon source: https://www.flaticon.com/authors/flat-icons, https://cdn-icons-png.flaticon.com/512/2534/2534888.png)
Accept The Rules is another modification for the minetest engine to let players read and accept the rules that the server administrator set.

## Features

- [x] Easy to use GUI (Graphical User Interface)
- [x] Adapt your own rules 
- [x] Keyword support
- [x] Kick player on wrong key
- [x] translation support
- [x] Ingame commands to review the rules
- [ ] Attemps 
- [ ] Admin control

## Why is it important to let the player read all the rules
 Normally players would just skip that part. They would either press escape or if the rules are written on signs dont read them. But if they have to search for a keyword in the rules/text, they have to atleast skim it. In addition to that they accepted the rules by pressing on a button and can be reminded by breaking a rule.
 
## Setup
1.  Download the mod or git clone it and place it in your mod directory
            ``git clone "https://gitlab.com/Tomracer/accepttherules.git" ``
2. Create a folder called locale in your wolds directory.
        Add your rules file(s) into that folder and call them ``language.txt`` 
        if you want to use a other file extension edit the init lua file and the corresponding 
        ``file_extension``  variable
3. Add the flowing parameter to your minetest.conf if want to edit them
    - ``rules_kick = true/false``

        Players get kicked if they enterd the wrong keyword or escaped the formspec
    - ``rules_show_new_player = true/false``

        Requires every new player to accept the rules in order to play.
        If they accepted them a corresponding priv will be granted

    - ``rules_keyword = string``

        The Keyword the player has to enter if rules_show_new_player is set to true.
        if empty only a accept button will be shown

    - ``rules_kick_reason = string``

        The reason that will be shown after the player got kicked

    - ``rules_accept_message = strin``

        The message that will be send to player after they accepted the rules.

        
4. Add the following line to your world.mt or set it to true if it is already present: 
        ``load_mod_accepttherules = true``

## License
 
 [MIT Copyright (c) 2022 tomraceror](https://gitlab.com/Tomracer/accepttherules/-/blob/master/license.txt)
 
