file_extension = ".txt"

minetest.register_privilege("Rules", {
	description = "Accepted the server rules",
    give_to_singleplayer = false,
    on_grant = function(player_name, granter_name)
        local accepted_string = minetest.settings:get("rules_accept_message") or "Rules accepted. Have fun playing"
        minetest.chat_send_player(player_name, accepted_string)
    end
    })

local function read_from_file(path)
    local file = io.open(path)
    local content = file:read("*a")
    file:close()
    return content
end

local function get_formspec(language)
    local dir = minetest.get_dir_list(minetest.get_worldpath().."/locale")
    local widget = "[]"
    local text = "No rules found"
    local lang_existance = false
    if #dir > 1 then
        widget = "dropdown[8.7,0.25;3,0.7;dropdown_selected;"
        if not language then 
            language = dir[1]
            lang_existance = true
        end
        for key, value in ipairs(dir) do
            if value == language then 
                lang_existance = true
            end
            widget = widget..value:sub(1,-5)..","
            if key == #dir then
                widget = widget:sub(1,-2)..";1]"
            end
        end
    end
    text = read_from_file(minetest.get_worldpath().."/locale/"..language)

    local formspec = {
        "formspec_version[4]",
        "size[12,12]",
        "label[5.3,0.5;Rules]",
        "textarea[0.6,1.2;10.8,10;rules_string;;"..text.."]",
        widget
    }
    return table.concat(formspec, ""), formspec
end

minetest.register_chatcommand("rules", {
    description = "Review the rules",
    func = function(player_name)
        local formspec_string = get_formspec()
        minetest.show_formspec(player_name, "server_rules", formspec_string)

    end
})

local function merge_formspec(basic_formspec_string, basic_formspec_table)
    table.remove(basic_formspec_table, 2)
    table.insert(basic_formspec_table, 2, "size[12,14]")
    table.insert(basic_formspec_table, "button[6.9,12.4;2.7,0.9;accept;accept]")
    table.insert(basic_formspec_table, "pwdfield[2.2,12.4;4.3,0.9;keyword_string;Keyword]")
    return table.concat(basic_formspec_table, "")
end

minetest.register_on_joinplayer(function(player, last_login)
    local status, privileges = minetest.check_player_privs(player, "Rules")
    local show_new_player = minetest.settings:get_bool("rules_show_new_player")
    if not status and show_new_player then
        minetest.show_formspec(player:get_player_name(), "server_accept_rules", merge_formspec(get_formspec()))
    end
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
    local player_name = player:get_player_name()
    
    if formname ~= "server_accept_rules" then
        if formname == "server_rules" and fields.dropdown_selected then
            minetest.show_formspec(player_name, formname, get_formspec(fields.dropdown_selected..file_extension))
        end
        return
    end

    if formname == "server_accept_rules" then
        local keyword = minetest.settings:get("rules_keyword")
        local kick = minetest.settings:get_bool("rules_kick")
        if fields.accept and fields.keyword_string == keyword then
            local privileges = minetest.get_player_privs(player_name)
            privileges.Rules = true
            minetest.set_player_privs(player_name, privileges)
            minetest.close_formspec(player_name, formname)
        elseif fields.accept and fields.keyword_string ~= keyword and kick then
            minetest.kick_player(player_name, minetest.settings:get("rules_kick_reason") or "Rules not accepted, wrong keyword!")
        elseif fields.dropdown_selected then
            minetest.show_formspec(player_name, formname, merge_formspec(get_formspec(fields.dropdown_selected..file_extension)))
        end
    end

end)